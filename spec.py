from openpyxl import load_workbook
from openpyxl.styles import Border, Side
from datetime import datetime
import os
import sys
import copy

unique_use_dict = []

template_spec = 'templates/template_shimko_spec.xlsx'


def element_strip(element):
    if element["DesRadComp"]:
        element["DesRadComp"] = element["DesRadComp"].strip()
    if element["Description"]:
        element["Description"] = element["Description"].strip()
    if element["ManPN"]:
        element["ManPN"] = element["ManPN"].strip()
    if element["Doc"]:
        element["Doc"] = element["Doc"].strip()

    return element


def bom_sort(ws):
    """Основная процедура сортировки перечня элементов"""
    prev_element = {
        "NUM": "",
        "DesRadComp": "",
        "Description": "",
        "ManPN": "",
        "PartNumber": "",
        "Doc": "",
        "RefDes": "",
        "Value": "",
        "QTY": ""
    }

    ref_des_start = ""

    # Обрабатываем BOM-файл и сортируем его в соответствии с перечнем элементов по ГОСТ
    for row in range(1, ws.max_row):
        cur_element = {
            "NUM": ws["A" + str(row + 1)].value,
            "DesRadComp": ws["B" + str(row + 1)].value,
            "Description": ws["C" + str(row + 1)].value,
            "ManPN": ws["D" + str(row + 1)].value,
            "PartNumber": ws["E" + str(row + 1)].value,
            "Doc": ws["F" + str(row + 1)].value,
            "RefDes": ws["G" + str(row + 1)].value,
            "Value": ws["H" + str(row + 1)].value,
            "QTY": ws["I" + str(row + 1)].value
        }

        # Аварийный выход, если по какой-то причине будет некорректно посчитано кол-во строк
        if not cur_element["NUM"]:
            break

        # Перед сравнением элементов, удаляем пробелы в начале и конце строк
        cur_element = element_strip(cur_element)
        # Проверяем уникальность компонента
        # Уникальный компонент - компонент с неповторяющимися PartNumber, Description и Value.
        # Таким образом, если хотя бы одно из этих свойств у текущего и предыдущего компонентов различается,
        # то текущий компонент уникальный
        if ((cur_element["PartNumber"] != prev_element["PartNumber"]) or
                (cur_element["Description"] != prev_element["Description"]) or
                (cur_element["Value"] != prev_element["Value"])):

            unique_use_dict.append(cur_element)
            ref_des_start = cur_element["RefDes"]

        else:
            unique_use_dict[len(unique_use_dict) - 1]["RefDes"] = ref_des_start + '-' + cur_element["RefDes"]
            unique_use_dict[len(unique_use_dict) - 1]["QTY"] += 1

        prev_element = cur_element.copy()


def bom_check(ws):
    if ws["B1"].value != "DesRadComp":
        print("BOM-файл не корректен! Не найдено поле [B1] DesRadComp...")
        return -1
    if ws["C1"].value != "Description":
        print("BOM-файл не корректен! Не найдено поле [C1] Description...")
        return -1
    if ws["D1"].value != "ManfPN":
        print("BOM-файл не корректен! Не найдено поле [D1] ManfPN...")
        return -1
    if ws["E1"].value != "Part Number":
        print("BOM-файл не корректен! Не найдено поле [E1] Part Number...")
        return -1
    if ws["F1"].value != "Documentation":
        print("BOM-файл не корректен! Не найдено поле [F1] Documentation...")
        return -1
    if ws["G1"].value != "Ref Designator":
        print("BOM-файл не корректен! Не найдено поле [G1] Ref Designator...")
        return -1
    if ws["H1"].value != "Value":
        print("BOM-файл не корректен! Не найдено поле [H1] Value...")
        return -1
    if ws["I1"].value != "QTY":
        print("BOM-файл не корректен! Не найдено поле [I1] QTY...")
        return -1

    return 0


def list_delete_same(list_of_components):
    """Удаляет повторяющиется элементы из списка"""
    while True:
        for cur_comp in range(0, len(list_of_components)):
            if list_of_components.count(list_of_components[cur_comp]) > 1:
                list_of_components.remove(list_of_components[cur_comp])
                break

        count = 0
        for cur_comp in range(0, len(list_of_components)):
            count += (list_of_components.count(list_of_components[cur_comp]))

        if count == len(list_of_components):  # Одинаковые компоненты удалены
            break


def convert_to_uf(element, prefix):
    if (prefix == 'pf') or (prefix == 'пф'):
        if element["Value"].endswith(prefix):
            val_list = element["Value"].split(prefix)
            val = '{0:f}'.format(float(val_list[0]) / 1000000)
            element["Value"] = f'{val}мкФ'
    elif (prefix == 'nf') or (prefix == 'нф'):
        if element["Value"].endswith(prefix):
            val_list = element["Value"].split(prefix)
            val = '{0:f}'.format(float(val_list[0]) / 1000)
            element["Value"] = f'{val}мкФ'

    return element["Value"]


def set_uf_val(element):
    if not element["Value"]:
        print("Error! No attribute Value")
        return

    element["Value"] = element["Value"].replace(' ', '')
    element["Value"] = element["Value"].replace(',', '.')
    element["Description"] = element["Description"].replace(' ', '')
    element["Description"] = element["Description"].replace(',', '.')

    # Устанавливаем одну и ту же приставку (мкФ)
    if element["Value"].endswith("uF"):
        element["Value"] = element["Value"].replace('uF', 'мкФ')

    # Добавляем приставку для целых и вещественных значений, если ее нет
    if element["Value"].replace('.',',1').isdigit():
        element["Value"] = element["Value"] + "мкФ"

    # Приводим к нижнему регистру для удобства сравнения
    element["Value"] = element["Value"].lower()

    element["Value"] = convert_to_uf(element, 'pf')
    element["Value"] = convert_to_uf(element, 'пф')
    element["Value"] = convert_to_uf(element, 'nf')
    element["Value"] = convert_to_uf(element, 'нф')


def convert_to_unit(element, prefix):
    """Переводит текущее значение резистора в условную единицу для сортировки.
    Значение в [Ом] делится на 1_000_000; в [кОм] - на 1_000; в [МОм] - на 1"""
    is_convert = False
    if element["Value"].endswith("om"):
        val_list = element["Value"].split("om")
        val = '{0:f}'.format(float(val_list[0])/1_000_000)
        element["Value"] = f'{val}U'
        is_convert = True
    elif element["Value"].endswith("ом"):
        val_list = element["Value"].split("ом")
        val = '{0:f}'.format(float(val_list[0]) / 1_000_000)
        element["Value"] = f'{val}U'
        is_convert = True
    elif (prefix == 'k') or (prefix == 'kom') or (prefix == 'к') or (prefix == 'ком'):
        if element["Value"].endswith(prefix):
            val_list = element["Value"].split(prefix)
            val = '{0:f}'.format(float(val_list[0]) / 1_000)
            element["Value"] = f'{val}U'
            is_convert = True
    elif (prefix == 'm') or (prefix == 'mom') or (prefix == 'м') or (prefix == 'мом'):
        print(element["Value"])
        if element["Value"].endswith(prefix):
            val_list = element["Value"].split(prefix)
            print(val_list)
            val = '{0:f}'.format(float(val_list[0]) / 1)
            element["Value"] = f'{val}U'
            is_convert = True

    return element["Value"], is_convert


def set_unit_val(element):
    element["Value"] = str(element["Value"])
    element["Value"] = element["Value"].replace(' ', '')
    element["Value"] = element["Value"].replace(',', '.')
    element["Description"] = element["Description"].replace(' ', '')
    element["Description"] = element["Description"].replace(',', '.')

    # Устанавливаем одну и ту же приставку (Ом)
    if element["Value"].endswith("Om"):
        element["Value"] = element["Value"].replace('Om', 'Ом')

    # Добавляем приставку для целых и вещественных значений, если ее нет
    if element["Value"].replace('.',',1').isdigit():
        element["Value"] = element["Value"] + "Ом"

    # Приводим к нижнему регистру для удобства сравнения
    element["Value"] = element["Value"].lower()

    is_convert = False
    element["Value"], is_convert = convert_to_unit(element, 'k')

    if not is_convert:
        element["Value"], is_convert = convert_to_unit(element, 'к')
    if not is_convert:
        element["Value"], is_convert = convert_to_unit(element, 'kom')
    if not is_convert:
        element["Value"], is_convert = convert_to_unit(element, 'ком')
    if not is_convert:
        element["Value"], is_convert = convert_to_unit(element, 'm')
    if not is_convert:
        element["Value"], is_convert = convert_to_unit(element, 'м')
    if not is_convert:
        element["Value"], is_convert = convert_to_unit(element, 'mom')
    if not is_convert:
        element["Value"] = convert_to_unit(element, 'мом')


def replace_c_description_val(element):
    """Поиск и замена текущей приставки в описании конденсатора на мкФ"""
    is_replace = False
    description_list = element["Description"].split('-')
    for cur_description in description_list:
        if ("uF" in cur_description) or ("nF" in cur_description) or ("pF" in cur_description)\
                    or ("мкФ" in cur_description) or ("нФ" in cur_description) or ("пФ" in cur_description):
            element["Description"] = element["Description"].replace(f'{cur_description}', f'{element["Value"]}')
            is_replace = True

    # Замена для компонента со стандартным описанием при отсутствии приставки
    if not is_replace:
        # description_list[1] - значение из описания
        element["Description"] = element["Description"].replace(f'{description_list[1]}', f'{element["Value"]}')


def replace_r_description_val(element):
    """Поиск и замена текущей приставки в описании резистора на Ом"""
    is_replace = False
    description_list = element["Description"].split('-')
    for cur_description in description_list:
        if ("Ом" in cur_description) or ("к" in cur_description) or ("кОм" in cur_description)\
                    or ("Om" in cur_description) or ("k" in cur_description) or ("kOm" in cur_description)\
                or ("К" in cur_description) or ("K" in cur_description) or ("M" in cur_description)\
                or ("MOm" in cur_description) or ("МОм" in cur_description):
            element["Description"] = element["Description"].replace(f'{cur_description}', f'{element["Value"]}')
            is_replace = True

    # Замена для компонента со стандартным описанием при отсутствии приставки
    if not is_replace:
        # description_list[1] - значение из описания
        element["Description"] = element["Description"].replace(f'{description_list[1]}', f'{element["Value"]}')


def set_description(element):
    description = ""
    # Для компонентов, отличных от резисторов и конденсаторов, добавляем к описанию назначение
    if (element["RefDes"][0] != 'C') and (element["RefDes"][0] != 'R'):
        des_rad = element["DesRadComp"]
        if des_rad:
            description += des_rad.strip()
            description += " "

    desc = element["Description"]
    if desc:
        description += desc.strip()
    else:
        man_pn = element["ManPN"]
        if man_pn:
            description += man_pn
        else:
            description += element["PartNumber"]

    if (element["RefDes"][0] == 'C') or (element["RefDes"][0] == 'R') or \
            (element["RefDes"][0] == 'L') or (element["RefDes"][0] == 'G') or (element["RefDes"][0] == 'Z'):
        value = element["Value"]
        if value and (not desc):
            description += "-"
            description += str(value)

    return description


def set_des_rad(element):
    des_rad = ""
    if element["RefDes"][0] == 'R':
        des_rad = "Резистор"
    elif element["RefDes"][0] == 'C':
        des_rad = "Конденсатор"
    else:
        des_rad = element["DesRadComp"]

    if not des_rad:
        des_rad = ""

    return des_rad


def set_doc(element):
    doc = element["Doc"]
    if not doc:
        doc = ""
    return doc.strip()


def check_count_row():
    """Проверяет валидность счетчика текущей строки"""
    global cur_list_name_row_cnt, cur_list_note_row_cnt, cur_list_pos_row_cnt
    # Числа 18, 24, 28 и.т.д - это значения нижних границ объединения 2-х и более строк в шаблоне!!!
    if (cur_list_name_row_cnt == 18) or (cur_list_name_row_cnt == 24) or (cur_list_name_row_cnt == 28) or \
            (cur_list_name_row_cnt == 32) or (cur_list_name_row_cnt == 37):
        cur_list_name_row_cnt += 1  # Стандартный инкремент объединения 2-х строк в шаблоне

    if (cur_list_note_row_cnt == 18) or (cur_list_note_row_cnt == 24) or (cur_list_note_row_cnt == 28) or \
            (cur_list_note_row_cnt == 32) or (cur_list_note_row_cnt == 37):
        cur_list_note_row_cnt += 1  # Стандартный инкремент объединения 2-х строк в шаблоне

    if (cur_list_pos_row_cnt == 18) or (cur_list_pos_row_cnt == 24) or (cur_list_pos_row_cnt == 28) or \
            (cur_list_pos_row_cnt == 32) or (cur_list_pos_row_cnt == 37):
        cur_list_pos_row_cnt += 1  # Стандартный инкремент объединения 2-х строк в шаблоне


def check_free_cell():
    """Проверяет, есть ли еще свободные ячейки. При необходимотси загружает новые листы"""
    global cur_list_name_row_cnt, cur_list_note_row_cnt, cur_list_pos_row_cnt,\
        max_of_rows, count_of_lists, wb_list_spec, ws_list_spec
    # Оставляем не менее 3-х свободных ячеек
    is_free_cell = True
    if ((max_of_rows - cur_list_name_row_cnt) < 6) or ((max_of_rows - cur_list_note_row_cnt) < 6):
        print("Лист " + str(count_of_lists) + " обработан!")
        # Места нет. Создаем новю книгу и лист из шаблона add
        print("Загружаем Лист " + str(count_of_lists + 1) + "...")
        wb_list_spec.append(load_workbook(template_spec))
        ws_list_spec.append(wb_list_spec[count_of_lists].active)
        # Новый лист создан. Пронумеруем лист и установим новые значения счетчиков
        ws_list_spec[count_of_lists]['O40'].value = start_list + count_of_lists
        count_of_lists += 1
        cur_list_name_row_cnt = 3
        cur_list_note_row_cnt = 3
        cur_list_pos_row_cnt = 3
        is_free_cell = False

    return is_free_cell


def update_spec_sheet(worksheet):
    border = Border(left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    top=Side(border_style='thin'),
                    bottom=Side(border_style='thin'),
                    vertical=Side(border_style='thin'),
                    horizontal=Side(border_style='thin'))
    for cellObj in worksheet['C1:O1']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['A18:B41']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['C41:J41']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['K39:N41']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['O39:O41']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(bottom=Side(border_style='hair'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    vertical=Side(border_style='thin'),
                    horizontal=Side(border_style='hair'))
    for cellObj in worksheet['C2:O38']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    top=Side(border_style='thin'),
                    vertical=Side(border_style='thin'))
    for cellObj in worksheet['C39:J39']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(top=Side(border_style='hair'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    bottom=Side(border_style='thin'),
                    vertical=Side(border_style='thin'))
    for cellObj in worksheet['C40:J40']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border


print("Загрузка шаблона...")
if not os.path.exists(template_spec):
    print("Ошибка открытия файла шаблона!!!")
    print("Файл " + template_spec + " не найден!!!")
    sys.exit()

print("Файл шаблона загружен!")
print("Загрузка BOM-файла...")
while True:
    print("Введите имя BOM-файла:")
    bom_file = input()
    if not bom_file.endswith(".xlsx"): # При необходимости добавляем расширение
        bom_file += ".xlsx"
    if os.path.exists(bom_file):
        break
    else:
        print("Файл " + bom_file + " не найден!")

# bom_file = 'Board1.xlsx'1
wb_bom = load_workbook(bom_file)
ws_bom = wb_bom.active
print("Файл " + bom_file + " загружен!")

# Проверяем корректность BOM-файла
print("Проверка корректности BOM-файла...")
if bom_check(ws_bom) == -1:
    sys.exit()
print("BOM-файл корректен!")

print("Сортировка BOM-файла...")
bom_sort(ws_bom)
print("BOM-файл обработан!")

# Копируем словарь уникальных компонентов
use_dict = unique_use_dict.copy()

# Расширяем описание компонента
for i in range(len(use_dict)):
    use_dict[i]["Description"] = set_description(use_dict[i])
    use_dict[i]["DesRadComp"] = set_des_rad(use_dict[i])
    use_dict[i]["Doc"] = set_doc(use_dict[i])

# Копия словаря для формирования отсортированного словаря
use_dict_unsorted = use_dict.copy()

# Словарь для хранения отсортированных компонентов
components_dict = {
    "C_LIST": [],
    "D_LIST": [],
    "G_LIST": [],
    "L_LIST": [],
    "Q_LIST": [],
    "R_LIST": [],
    "S_LIST": [],
    "T_LIST": [],
    "V_LIST": [],
    "X_LIST": [],
    "Z_LIST": []
}
# Список для хранения словарей конденсаторов
c_list = []
# Список для хранения словарей резисторов
r_list = []
# Список для хранения словарей с форматированными описаниями конденсаторов
c_description_list = []
# Список для хранения словарей с форматированными описаниями резисторов
r_description_list = []

# Группируем списки компонентов по RefDes
print("Группируем списки компонентов...")
use_dict.reverse()
while len(use_dict):
    cur_component = use_dict.pop()
    cur_ref_des = cur_component["RefDes"][0]
    # Словарь конденсаторов. Исп-ся для хранения описания и значения конденсаторов
    c_dict = {
        "Description": "",
        "Value": ""
    }
    # Словарь резисторов. Исп-ся для хранения описания и значения резисторов
    r_dict = {
        "Description": "",
        "Value": ""
    }
    if cur_ref_des == 'C':
        c_dict["Description"] = cur_component["Description"]
        c_dict["Value"] = cur_component["Value"]
        c_list.append(c_dict)
        # components_dict["C_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'D':
        components_dict["D_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'G':
        components_dict["G_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'L':
        components_dict["L_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'Q':
        components_dict["Q_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'R':
        r_dict["Description"] = cur_component["Description"]
        r_dict["Value"] = cur_component["Value"]
        r_list.append(r_dict)
        # components_dict["R_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'S':
        components_dict["S_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'T':
        components_dict["T_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'V':
        components_dict["V_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'X':
        components_dict["X_LIST"].append(cur_component["Description"])
    elif cur_ref_des == 'Z':
        components_dict["Z_LIST"].append(cur_component["Description"])

# Почему-то не работает метод copy() для списка словарей...
# Используем метод deepcopy() модуля copy для полного копирования списка
c_list_mod = copy.deepcopy(c_list)
r_list_mod = copy.deepcopy(r_list)

# Устанавливаем одинаковые приставки для значений конденсаторов
for cur_dict in c_list_mod:
    set_uf_val(cur_dict)

# Устанавливаем одинаковые приставки для значений резисторов
for cur_dict in r_list_mod:
    set_unit_val(cur_dict)

# Меняем приставки на мкФ в описании конденсаторов
for cur_dict in c_list_mod:
    replace_c_description_val(cur_dict)

# Меняем приставки на Ом в описании резисторов
for cur_dict in r_list_mod:
    replace_r_description_val(cur_dict)

# Заполняем список для хранения словарей с описаниями конденсаторов
while len(c_list) and len(c_list_mod):
    c_description_dict = {
        "DESCRIPTION_STD": "",
        "DESCRIPTION_MOD": ""
    }
    cur_c_description_std = c_list.pop()["Description"]
    cur_c_description_mod = c_list_mod.pop()["Description"]

    c_description_dict["DESCRIPTION_STD"] = cur_c_description_std
    c_description_dict["DESCRIPTION_MOD"] = cur_c_description_mod
    c_description_list.append(c_description_dict)

# print("c_description_list")
# for x in c_description_list:
#     print(x)

# Заполняем список для хранения словарей с описаниями резисторов
while len(r_list) and len(r_list_mod):
    r_description_dict = {
        "DESCRIPTION_STD": "",
        "DESCRIPTION_MOD": ""
    }
    cur_r_description_std = r_list.pop()["Description"]
    cur_r_description_mod = r_list_mod.pop()["Description"]

    r_description_dict["DESCRIPTION_STD"] = cur_r_description_std
    r_description_dict["DESCRIPTION_MOD"] = cur_r_description_mod
    r_description_list.append(r_description_dict)

# Сортируем словарь конденсаторов по модифицированному описанию
c_description_list_sorted = sorted(c_description_list, key=lambda c: c["DESCRIPTION_MOD"])

# Сортируем словарь резисторов по модифицированному описанию
r_description_list_sorted = sorted(r_description_list, key=lambda r: r["DESCRIPTION_MOD"])

# Заполняем спиок отсортированных конденсаторов
for cur_c_description in c_description_list_sorted:
    components_dict["C_LIST"].append(cur_c_description["DESCRIPTION_STD"])

# Заполняем спиок отсортированных резисторов
for cur_r_description in r_description_list_sorted:
    components_dict["R_LIST"].append(cur_r_description["DESCRIPTION_STD"])

# Удаляем повторяющиеся элементы в списках
list_delete_same(components_dict["C_LIST"])
list_delete_same(components_dict["D_LIST"])
list_delete_same(components_dict["G_LIST"])
list_delete_same(components_dict["L_LIST"])
list_delete_same(components_dict["Q_LIST"])
list_delete_same(components_dict["R_LIST"])
list_delete_same(components_dict["S_LIST"])
list_delete_same(components_dict["T_LIST"])
list_delete_same(components_dict["V_LIST"])
list_delete_same(components_dict["X_LIST"])
list_delete_same(components_dict["Z_LIST"])

# Сортируем списки компонентов
print("Сортируем списки компонентов...")
# components_dict["C_LIST"].sort()  # Список C_LIST уже отсортирован !!!
components_dict["D_LIST"].sort()
components_dict["G_LIST"].sort()
components_dict["L_LIST"].sort()
components_dict["Q_LIST"].sort()
# components_dict["R_LIST"].sort()  # Список R_LIST уже отсортирован !!!
components_dict["S_LIST"].sort()
components_dict["T_LIST"].sort()
components_dict["V_LIST"].sort()
components_dict["X_LIST"].sort()
components_dict["Z_LIST"].sort()

# Формируем единный отсортированный список компонентов
components_list = []
components_list.extend(components_dict["C_LIST"])
components_list.extend(components_dict["D_LIST"])
components_list.extend(components_dict["G_LIST"])
components_list.extend(components_dict["L_LIST"])
components_list.extend(components_dict["Q_LIST"])
components_list.extend(components_dict["R_LIST"])
components_list.extend(components_dict["S_LIST"])
components_list.extend(components_dict["T_LIST"])
components_list.extend(components_dict["V_LIST"])
components_list.extend(components_dict["X_LIST"])
components_list.extend(components_dict["Z_LIST"])

# Формируем отсортированный словарь компонентов
print("Формируем словарь компонентов...")
components_dict_sorted = []
components_list.reverse()
while len(components_list):
    component_cur = components_list.pop()
    for cur_element in use_dict_unsorted:
        if cur_element["Description"] == component_cur:
            components_dict_sorted.append(cur_element)

# for element in components_dict_sorted:
#     print(element)

# Список для хранения книг спецификации
wb_list_spec = []
# Список для хранения листов спецификации
ws_list_spec = []

# Добавляем в спиок 1-ую книгу и 1-ый лист
wb_list_spec.append(load_workbook(template_spec))
ws_list_spec.append(wb_list_spec[0].active)
ws_list_spec[0]["L2"].value = "Прочие изделия"

# Счетчик текущей строки для поля Поз.
cur_list_pos_row_cnt = 4
# Текущее значение для поля Поз.
cur_list_pos_val = 8
# Счетчик текущей строки для поля Наименование.
cur_list_name_row_cnt = 3
# Счетчик текущей строки для поля Примечание.
cur_list_note_row_cnt = 3
# Счетчик текущей строки для поля Кол.
cur_list_qty_row_cnt = 3

# Максимальное кол-во строк в листе. 38 - для шаблона template_shomko_spec.xlsx
max_of_rows = 38
# Кол-во листов
count_of_lists = 1
# Номер стартового листа
start_list = 2
# Максимальная ширина поля "Наименование", в символах
max_of_symbol_name = 33
# Максимальная ширина поля "Примечание", в символах
max_of_symbol_note = 9

is_update_list = False

prev_element = {
    "DesRadComp": "",
    "Description": "",
    "ManPN": "",
    "PartNumber": "",
    "RefDes": "A",
    "Doc": "",
    "Value": "",
    "QTY": ""
}
prev_ref_des = prev_element["RefDes"][0]

components_dict_sorted.reverse()
# Главный цикл заполнения листов
print("Заполняем поля спецификации...")
while len(components_dict_sorted):
    print("Обработка Листа " + str(count_of_lists) + "...")

    cur_element = components_dict_sorted.pop()
    cur_ref_des = cur_element["RefDes"][0]
    # Оставляем пустую строчку между разными типами компонентов
    if cur_ref_des != prev_ref_des:
        cur_list_name_row_cnt += 1
        cur_list_note_row_cnt += 1

    if (cur_element["Description"] != prev_element["Description"]) or is_update_list:
        is_update_list = False
        if prev_element["Description"]:
            print("...поля для " + prev_element["Description"] + " заполнены!")
        print("Заполняем поля для " + cur_element["Description"] + "...")

        # TODO: Убрать комменты, если необходима пустая строка между компонентами одного типа
        cur_list_name_row_cnt += 1
        cur_list_note_row_cnt += 1

        # Заполняем поле Наименование (Тип компонента) и Поз.
        # Сначала проверяем валидность счетчика текущей строки и свободны ли ячейки
        check_count_row()
        check_free_cell()
        cur_list_pos_row_cnt = cur_list_name_row_cnt
        cur_cell = 'F' + str(cur_list_pos_row_cnt)
        ws_list_spec[count_of_lists - 1][cur_cell].value = cur_list_pos_val
        cur_list_pos_val += 1

        cur_list_note_row_cnt = cur_list_name_row_cnt
        cur_list_qty_row_cnt = cur_list_name_row_cnt
        cur_cell = 'L' + str(cur_list_name_row_cnt)
        # Для компонентов, отличных от резисторов и конденсаторов, заполняем поле Наименование (Описание компонента)
        if (cur_ref_des != 'C') and (cur_ref_des != 'R'):
            if len(cur_element["Description"]) > max_of_symbol_name:  # Поле описания компонента слишком широкое
                # Сначала пишем Тип компонента
                ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["DesRadComp"]
                cur_list_name_row_cnt += 1
                check_count_row()
                # Затем Описание (Удалив тип!!!)
                cur_cell = 'L' + str(cur_list_name_row_cnt)
                description = cur_element["Description"].replace(f'{cur_element["DesRadComp"]}', "")
                # Удаляем пробелы в начале строки, оставшиеся после удаления типа!
                description = description.lstrip()
                ws_list_spec[count_of_lists - 1][cur_cell].value = description
                # TODO: Оставляем пустую строчку, если поле описания слишком большое
                # if len(description) >= max_of_symbol_name:
                    # cur_list_name_row_cnt += 1
            else:
                ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["Description"]
        else:  # Для резисторов и конденсаторов
            if (len(cur_element["DesRadComp"]) + len(cur_element["Description"]) + 1) > max_of_symbol_name:
                # Поле описания компонента слишком широкое!
                # Сначала заполняем поле Наименование (Тип компонента)...
                ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["DesRadComp"]
                cur_list_name_row_cnt += 1
                check_count_row()
                # ...затем поле Наименование (Описание компонента)
                cur_cell = 'L' + str(cur_list_name_row_cnt)
                ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["Description"]
                # TODO: Оставляем пустую строчку, если поле описания слишком большое
                # if len(cur_element["Description"]) >= max_of_symbol_name:
                #     cur_list_name_row_cnt += 1

            else:  # Можно записать в одну строчку
                ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["DesRadComp"] + " " + \
                                                                   cur_element["Description"]

        cur_list_name_row_cnt += 1

        # Заполняем поле Кол.
        cur_cell = 'M' + str(cur_list_qty_row_cnt)
        ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["QTY"]
        # Заполняем поле Примечание
        cur_cell = 'N' + str(cur_list_note_row_cnt)
        ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["RefDes"]

        if cur_element["Doc"]:
            # Заполняем поле Наименование (ТУ)
            check_count_row()
            cur_cell = 'L' + str(cur_list_name_row_cnt)
            ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["Doc"]
            cur_list_name_row_cnt += 1
    else:  # Тот же компонент
        # Добавляем запятую в поле Примечание...
        cur_cell = 'N' + str(cur_list_note_row_cnt)
        cur_val = ws_list_spec[count_of_lists - 1][cur_cell].value
        ws_list_spec[count_of_lists - 1][cur_cell].value = str(cur_val) + ', '
        # ... и текущее значение элемента
        if len(cur_val) + len(cur_element["RefDes"]) > max_of_symbol_note:
            cur_list_note_row_cnt += 1
        else:
            cur_val = ws_list_spec[count_of_lists - 1][cur_cell].value
            cur_element["RefDes"] = str(cur_val) + cur_element["RefDes"]

        check_count_row()
        cur_cell = 'N' + str(cur_list_note_row_cnt)
        ws_list_spec[count_of_lists - 1][cur_cell].value = cur_element["RefDes"]
        # ... и прибавляем счетчик кол-ва элементов
        cur_cell = 'M' + str(cur_list_qty_row_cnt)
        cur_val = ws_list_spec[count_of_lists - 1][cur_cell].value
        ws_list_spec[count_of_lists - 1][cur_cell].value = int(cur_element["QTY"])+int(cur_val)

        if cur_list_note_row_cnt >= cur_list_name_row_cnt:
            cur_list_name_row_cnt = cur_list_note_row_cnt + 1
            # Здесь возможен случай, когда кол-во строк превысит диапазон листа
            # Проверим, что еще есть место и при необходимости обновим лист...
            if not check_free_cell():
                is_update_list = True;

    prev_element = cur_element
    prev_ref_des = cur_ref_des
    check_count_row()

print("Поля спецификации заполнены!")
print("Общее кол-во листов: " + str(count_of_lists))

# Нумеруем 1-ый лист
ws_list_spec[0]['O40'].value = start_list

# Обновляем файлы, чтобы восстановить исходные стили шаблонов
print("Обновляем файлы...")
for i in range(len(ws_list_spec)):
    update_spec_sheet(ws_list_spec[i])
print("Файла обновлены!")

# Создаем дирректорию для хранения файлов
datetime_now = datetime.today().strftime("%Y-%m-%d_%H-%M-%S")
if not os.path.exists(datetime_now):
    os.mkdir(datetime_now)
os.chdir(datetime_now)

# Сохраняем файлы
print("Сохраняем документацию в папку " + str(datetime_now) + "...")
for i in range(len(ws_list_spec)):
    wb_list_spec[i].save("list_of_spec_" + str(i) + ".xlsx")
print("Документация сохранена!")

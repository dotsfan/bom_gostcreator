from openpyxl import load_workbook
from openpyxl.styles import Border, Side
from datetime import datetime
import os
import sys

unique_use_dict = []
component_description = []

# Словарь кол-ва компонентов
component_dict = {
    "Z_QTY": 0,  # Генераторы Z (Добавлено для совместимости. По ГОСТ д.б. "G")
    "X_QTY": 0,  # Разъемы
    "V_QTY": 0,  # Диоды (Добавлено для совместимости. По ГОСТ д.б. "D")
    "T_QTY": 0,  # Трансформаторы
    "S_QTY": 0,  # Кнопки, переключатели цепи управления, сигнализации, измерения
    "R_QTY": 0,  # Резисторы
    "Q_QTY": 0,  # Выключатели цепи питания
    "L_QTY": 0,  # Индуктивности
    "G_QTY": 0,  # Генераторы G
    "D_QTY": 0,  # Микросхемы
    "C_QTY": 0   # Конденсаторы
}


def get_man_pn(element):
    if not element["ManPN"]:
        return element["PartNumber"]

    return element["ManPN"].strip()


def get_doc(element):
    if not element["Doc"]:
        return ""

    return element["Doc"].strip()


def get_component_qty(description_list):
    global component_dict
    """Устанавливает словарь кол-ва компонентов уникальных по ManPN/Doc/PartNumber в списке наименований"""
    for x in description_list:
        if x.find("Конденсаторы") >= 0:
            component_dict["C_QTY"] += 1
        elif x.find("Микросхемы") >= 0:
            component_dict["D_QTY"] += 1
        elif x.find("Генераторы G") >= 0:
            component_dict["G_QTY"] += 1
        elif x.find("Индуктивности") >= 0:
            component_dict["L_QTY"] += 1
        elif x.find("Выключатели цепи питания") >= 0:
            component_dict["Q_QTY"] += 1
        elif x.find("Резисторы") >= 0:
            component_dict["R_QTY"] += 1
        elif x.find("Кнопки, переключатели") >= 0:
            component_dict["S_QTY"] += 1
        elif x.find("Трансформаторы") >= 0:
            component_dict["T_QTY"] += 1
        elif x.find("Диоды") >= 0:
            component_dict["V_QTY"] += 1
        elif x.find("Разъемы") >= 0:
            component_dict["X_QTY"] += 1
        elif x.find("Генераторы Z") >= 0:
            component_dict["Z_QTY"] += 1


def get_description(element):
    """Формирует строку для поля Наименование """
    description = ""
    if (element["RefDes"][0] != 'R') and (element["RefDes"][0] != 'C'):
        des_rad = element["DesRadComp"]
        if des_rad:
            description += des_rad.strip()
            description += " "
        desc = element["Description"]
        if desc:
            description += desc.strip()
        else:
            man_pn = element["ManPN"]
            if man_pn:
                description += man_pn.strip()
            else:
                description += element["PartNumber"]
        description += " "
        doc = element["Doc"]
        if doc:
            description += doc.strip()
    else:
        description = element["Description"]
        if not description:
            description = element["ManPN"]
            if not description:
                description = element["PartNumber"]

    description += " "
    if (element["RefDes"][0] == 'C') or (element["RefDes"][0] == 'R') or \
            (element["RefDes"][0] == 'L') or (element["RefDes"][0] == 'G') or (element["RefDes"][0] == 'Z'):
        value = element["Value"]
        # Добавляем значение, если нет описания
        if value and not element["Description"]:
            description += str(value)

    return description


def element_strip(element):
    if element["DesRadComp"]:
        element["DesRadComp"] = element["DesRadComp"].strip()
    if element["Description"]:
        element["Description"] = element["Description"].strip()
    if element["ManPN"]:
        element["ManPN"] = element["ManPN"].strip()
    if element["Doc"]:
        element["Doc"] = element["Doc"].strip()

    return element


def bom_sort(ws):
    """Основная процедура сортировки перечня элементов"""
    prev_element = {
        "NUM": "",
        "DesRadComp": "",
        "Description": "",
        "ManPN": "",
        "PartNumber": "",
        "Doc": "",
        "RefDes": "",
        "Value": "",
        "QTY": ""
    }

    ref_des_start = ""

    # Обрабатываем BOM-файл и сортируем его в соответствии с перечнем элементов по ГОСТ
    for row in range(1, ws.max_row):
        cur_element = {
            "NUM": ws["A" + str(row + 1)].value,
            "DesRadComp": ws["B" + str(row + 1)].value,
            "Description": ws["C" + str(row + 1)].value,
            "ManPN": ws["D" + str(row + 1)].value,
            "PartNumber": ws["E" + str(row + 1)].value,
            "Doc": ws["F" + str(row + 1)].value,
            "RefDes": ws["G" + str(row + 1)].value,
            "Value": ws["H" + str(row + 1)].value,
            "QTY": ws["I" + str(row + 1)].value
        }

        # Аварийный выход, если по какой-то причине будет некорректно посчитано кол-во строк
        if not cur_element["NUM"]:
            break

        # Перед сравнением элементов, удаляем пробелы в начале и конце строк
        cur_element = element_strip(cur_element)
        # Проверяем уникальность компонента
        # Уникальный компонент - компонент с неповторяющимися PartNumber, Description и Value.
        # Таким образом, если хотя бы одно из этих свойств у текущего и предыдущего компонентов различается,
        # то текущий компонент уникальный
        if ((cur_element["PartNumber"] != prev_element["PartNumber"]) or
                (cur_element["Description"] != prev_element["Description"]) or
                (cur_element["Value"] != prev_element["Value"])):

            unique_use_dict.append(cur_element)
            ref_des_start = cur_element["RefDes"]

            # Формируем список компонентов по ManPN/Doc/PartNumber
            if ((cur_element["Doc"] != prev_element["Doc"]) or (cur_element["ManPN"] != prev_element["ManPN"]) or
                    (cur_element["PartNumber"] != prev_element["PartNumber"])):
                if cur_element["RefDes"][0] == "C":
                    component_description.append("Конденсаторы " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "D":
                    component_description.append("Микросхемы " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "G":
                    component_description.append("Генераторы G " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "L":
                    component_description.append("Индуктивности " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "Q":
                    component_description.append("Выключатели цепи питания " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "R":
                    component_description.append("Резисторы " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "S":
                    component_description.append("Кнопки, переключатели " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "T":
                    component_description.append("Трансформаторы " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "V":
                    component_description.append("Диоды " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "X":
                    component_description.append("Разъемы " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
                elif cur_element["RefDes"][0] == "Z":
                    component_description.append("Генераторы Z " + get_man_pn(cur_element) + " " +
                                                 get_doc(cur_element))
        else:
            unique_use_dict[len(unique_use_dict) - 1]["RefDes"] = ref_des_start + '-' + cur_element["RefDes"]
            unique_use_dict[len(unique_use_dict) - 1]["QTY"] += 1

        prev_element = cur_element.copy()


def bom_check(ws):
    if ws["B1"].value != "DesRadComp":
        print("BOM-файл не корректен! Не найдено поле [B1] DesRadComp...")
        return -1
    if ws["C1"].value != "Description":
        print("BOM-файл не корректен! Не найдено поле [C1] Description...")
        return -1
    if ws["D1"].value != "ManfPN":
        print("BOM-файл не корректен! Не найдено поле [D1] ManfPN...")
        return -1
    if ws["E1"].value != "Part Number":
        print("BOM-файл не корректен! Не найдено поле [E1] Part Number...")
        return -1
    if ws["F1"].value != "Documentation":
        print("BOM-файл не корректен! Не найдено поле [F1] Documentation...")
        return -1
    if ws["G1"].value != "Ref Designator":
        print("BOM-файл не корректен! Не найдено поле [G1] Ref Designator...")
        return -1
    if ws["H1"].value != "Value":
        print("BOM-файл не корректен! Не найдено поле [H1] Value...")
        return -1
    if ws["I1"].value != "QTY":
        print("BOM-файл не корректен! Не найдено поле [I1] QTY...")
        return -1

    return 0


def list_delete_same(list_of_components):
    """Удаляет повторяющиется элементы из списка"""
    while True:
        for cur_comp in range(0, len(list_of_components)):
            if list_of_components.count(list_of_components[cur_comp]) > 1:
                list_of_components.remove(list_of_components[cur_comp])
                break

        count = 0
        for cur_comp in range(0, len(list_of_components)):
            count += (list_of_components.count(list_of_components[cur_comp]))

        if count == len(list_of_components):  # Одинаковые компоненты удалены
            break


def check_count_row():
    """Проверяет валидность счетчика текущей строки"""
    global cur_list_row_cnt
    # Числа 8, 17, 20 и.т.д - это значения нижних границ объединения 2-х и более строк в шаблоне!!!
    if (cur_list_row_cnt == 8) or (cur_list_row_cnt == 17) or (cur_list_row_cnt == 20) or \
            (cur_list_row_cnt == 26) or (cur_list_row_cnt == 30) or (cur_list_row_cnt == 34) or \
            (cur_list_row_cnt == 35) or (cur_list_row_cnt == 40):
        if cur_list_row_cnt == 34:
            cur_list_row_cnt = 36  # Инкремент 2, т.к это средняя граница объединения 3-х строк в шаблоне !!!
        else:
            cur_list_row_cnt += 1  # Стандартный инкремент объединения 2-х строк в шаблоне


def check_free_cell():
    """Проверяет, есть ли еще свободные ячейки. При необходимотси загружает новые листы"""
    global cur_list_row_cnt, max_of_rows, count_of_lists, wb_list, ws_list
    if cur_list_row_cnt > max_of_rows:
        print("Лист " + str(count_of_lists) + " обработан!")
        # Места нет. Создаем новю книгу и лист из шаблона add
        print("Загружаем Лист " + str(count_of_lists + 1) + "...")
        wb_list.append(load_workbook(template_add))
        ws_list.append(wb_list[count_of_lists].active)
        # Новый лист создан. Пронумеруем лист и установим новые значения счетчиков
        ws_list[count_of_lists]['T45'].value = count_of_lists + 1
        count_of_lists += 1
        cur_list_row_cnt = 2
        max_of_rows = 41


def update_add_sheet(worksheet):
    border = Border(left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    top=Side(border_style='thin'),
                    bottom=Side(border_style='thin'),
                    vertical=Side(border_style='thin'),
                    horizontal=Side(border_style='thin'))
    for cellObj in worksheet['C1:T1']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['A20:B46']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['C46:I46']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['J43:S46']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['T43:T46']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(bottom=Side(border_style='hair'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    vertical=Side(border_style='thin'),
                    horizontal=Side(border_style='hair'))
    for cellObj in worksheet['C2:T41']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(top=Side(border_style='thin'),
                    bottom=Side(border_style='thin'))
    for cellObj in worksheet['C42:T42']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    top=Side(border_style='thin'),
                    vertical=Side(border_style='thin'))
    for cellObj in worksheet['C43:I43']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(top=Side(border_style='hair'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    bottom=Side(border_style='thin'),
                    vertical=Side(border_style='thin'))
    for cellObj in worksheet['C44:I45']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border


def update_main_sheet(worksheet):
    # update_common_sheet(worksheet)

    border = Border(left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    top=Side(border_style='thin'),
                    bottom=Side(border_style='thin'),
                    vertical=Side(border_style='thin'),
                    horizontal=Side(border_style='thin'))
    for cellObj in worksheet['A1:B16']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['C1:S1']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['A20:B44']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['J34:S36']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['C39:I39']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['J37:S39']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['J40:L44']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['M42:S44']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(bottom=Side(border_style='hair'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    vertical=Side(border_style='thin'),
                    horizontal=Side(border_style='hair'))
    for cellObj in worksheet['C2:S32']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['C40:I44']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(top=Side(border_style='thin'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'))
    for cellObj in worksheet['C33:S33']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(bottom=Side(border_style='thin'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'))
    for cellObj in worksheet['C34:I36']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    top=Side(border_style='thin'),
                    vertical=Side(border_style='thin'))
    for cellObj in worksheet['C37:I37']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(top=Side(border_style='hair'),
                    left=Side(border_style='thin'),
                    right=Side(border_style='thin'),
                    bottom=Side(border_style='thin'),
                    vertical=Side(border_style='thin'))
    for cellObj in worksheet['C38:I38']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border
    for cellObj in worksheet['C44:I44']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border

    border = Border(top=Side(border_style='thin'),
                    right=Side(border_style='hair'))
    for cellObj in worksheet['M41:Q41']:
        for cell in cellObj:
            worksheet[cell.coordinate].border = border


template_main = 'templates/template_shimko_main.xlsx'
template_add = 'templates/template_shimko_add.xlsx'

print("Загрузка шаблонов...")
if not os.path.exists(template_main):
    print("Ошибка открытия файла шаблона!!!")
    print("Файл " + template_main + " не найден!!!")
    sys.exit()
if not os.path.exists(template_add):
    print("Ошибка открытия файла шаблона!!!")
    print("Файл " + template_add + " не найден!!!")
    sys.exit()

print("Файлы шаблонов загружены!")
print("Загрузка BOM-файла...")
while True:
    print("Введите имя BOM-файла:")
    bom_file = input()
    if not bom_file.endswith(".xlsx"): # При необходимости добавляем расширение
        bom_file += ".xlsx"
    if os.path.exists(bom_file):
        break
    else:
        print("Файл " + bom_file + " не найден!")

# bom_file = 'Board1.xlsx'
wb_bom = load_workbook(bom_file)
ws_bom = wb_bom.active
print("Файл " + bom_file + " загружен!")

# Проверяем корректность BOM-файла
print("Проверка корректности BOM-файла...")
if bom_check(ws_bom) == -1:
    sys.exit()
print("BOM-файл корректен!")

print("Сортировка BOM-файла...")
bom_sort(ws_bom)
print("BOM-файл обработан!")

list_delete_same(component_description)

# Выводим список всех уникальных компонентов
print("***********************")
print("Список компонентов...")
for comp in component_description:
    print(comp)
print("***********************")

# print("***********************")
# print("Словарь компонентов...")
get_component_qty(component_description)
# print(component_dict)
# print("***********************")

# Список для хранения книг перечня элементов
wb_list = []
# Список для хранения листов перечня элементов
ws_list = []

# Добавляем в спиок 1-ую книгу и 1-ый лист
wb_list.append(load_workbook(template_main))
ws_list.append(wb_list[0].active)

# Делаем реверс списков для того, чтобы pop() возвращал первое добавленное в список значение
unique_use_dict.reverse()
component_description.reverse()

cur_component = unique_use_dict.pop()
cur_ref_des = cur_component["RefDes"]
prev_ref_des = cur_ref_des

# Счетчик текущей строки. Начальное значение - 2 (номер 1-ой строки для заполнения)
cur_list_row_cnt = 2
# Максимальное кол-во строк в листе. 32 - для 1-ого, 41 - для последующих
max_of_rows = 32
# Кол-во листов
count_of_lists = 1

is_unique_dict_empty = 0

# Главный цикл заполнения листов
print("Заполняем поля перечня элементов...")
while len(component_dict):
    print("Обработка Листа " + str(count_of_lists) + "...")
    # Получаем пару ключ-значение из словаря
    cur_key_val = component_dict.popitem()
    # print(cur_key_val)
    # print("len component_dict = " + str(len(component_dict)))

    # Определяем тип ключа
    # cur_key_val[0] - ключ из словаря component_dict
    # Соответственно, cur_key_val[0][0] - 1-ая буква ключа
    if (cur_key_val[0][0] == 'C') or (cur_key_val[0][0] == 'R'):  # Для конденсаторов или резисторов
        # Заполняем общее описание компонентов (поле "Наименование" листа) из списка "component_description"
        # (Цикл заполнения общего описания)
        for cnt_val in range(cur_key_val[1]):  # cur_key_val[1] - значение ключа из словаря component_dict
            # Сначала проверяем валидность счетчика текущей строки
            check_count_row()
            # Затем проверяем, есть ли еще свободные ячейки
            check_free_cell()
            # Заполняем описание
            if cur_key_val[0][0] == 'C':
                print("Заполняем общее описание конденсаторов...")
            elif cur_key_val[0][0] == 'R':
                print("Заполняем общее описание резисторов...")

            cur_component_desc = component_description.pop()
            cur_cell = 'G' + str(cur_list_row_cnt)
            # print("cur_ceil = " + cur_ceil)
            ws_list[count_of_lists - 1][cur_cell].value = cur_component_desc
            cur_list_row_cnt += 1

        if cur_key_val[0][0] == 'C':
            print("Общее описание конденсаторов заполнено!")
        elif cur_key_val[0][0] == 'R':
            print("Общее описание резисторов заполнено!")
    else:  # Для компонентов, отличных от конденсаторов и резисторовв
        for cnt_val in range(cur_key_val[1]):  # cur_key_val[1] - значение ключа из словаря component_dict
            print("Пропускаем общее описание " + cur_key_val[0][0] + "!")
            cur_component_desc = component_description.pop()

    if cur_key_val[1] == 0:
        print("Компонент " + cur_key_val[0][0] + " отсутствует в BOM-файле!")
        print("Пропускаем заполнение полей для " + cur_key_val[0][0] + "!")
        continue

    # Заполняем поля Поз.обозначение/Наименование/Кол.
    # (Цикл заполнения основных полей)
    while (cur_ref_des[0] == prev_ref_des[0]) and (is_unique_dict_empty == 0):
        print("Заполняем поля Поз.обозначение / Наименование / Кол. для " + cur_ref_des + "...")
        # Сначала проверяем валидность счетчика текущей строки
        check_count_row()
        # Затем проверяем, есть ли еще свободные ячейки
        check_free_cell()
        # Заполняем поле Поз.обозначение
        cur_cell = 'E' + str(cur_list_row_cnt)
        ws_list[count_of_lists - 1][cur_cell].value = cur_ref_des
        # Заполняем поле Наименование
        cur_cell = 'G' + str(cur_list_row_cnt)
        # print("cur_ceil = " + cur_ceil)
        ws_list[count_of_lists - 1][cur_cell].value = get_description(cur_component)
        # Заполняем поле Кол.
        cur_cell = 'N' + str(cur_list_row_cnt)
        ws_list[count_of_lists - 1][cur_cell].value = cur_component["QTY"]
        cur_list_row_cnt += 1

        print("...поля для " + cur_ref_des + " заполнены!")

        prev_ref_des = cur_ref_des

        if len(unique_use_dict) == 0:
            print("Словарь уникальных компонентов пуст!")
            is_unique_dict_empty = 1
            break

        # Считываем следующий уникальный компонент
        cur_component = unique_use_dict.pop()
        cur_ref_des = cur_component["RefDes"]

    # После выхода из Цикла заполнения основных полей,
    # устанавливаем текущий компонент предыдущим,
    # чтобы снова попасть в данный цикл на следующей итерации выборки из словаря кол-ва компонентов
    prev_ref_des = cur_ref_des
    # Оставляем пустую строчку перед заполнением новых типов компонентов (RefDes)
    check_count_row()
    cur_list_row_cnt += 1

print("Поля перечня элементов заполнены!")
print("Общее кол-во листов: " + str(count_of_lists))

# Все листы заполнены. Определяем общее кол-во
ws_list[0]['S41'].value = count_of_lists
# Нумеруем 1-ый лист
ws_list[0]['R41'].value = 1

# Обновляем файлы, чтобы восстановить исходные стили шаблонов
print("Обновляем файлы...")
update_main_sheet(ws_list[0])
for i in range(1, len(ws_list)):
    update_add_sheet(ws_list[i])
print("Файла обновлены!")

# Создаем дирректорию для хранения файлов
datetime_now = datetime.today().strftime("%Y-%m-%d_%H-%M-%S")
if not os.path.exists(datetime_now):
    os.mkdir(datetime_now)
os.chdir(datetime_now)

# Сохраняем файлы
print("Сохраняем документацию в папку " + str(datetime_now) + "...")
for i in range(len(wb_list)):
    wb_list[i].save("list_of_elements_" + str(i) + ".xlsx")
print("Документация сохранена!")
